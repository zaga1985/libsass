FROM ubuntu
RUN apt update \
    && apt install git gcc g++ make cmake ruby pkgconf -y
RUN cd /usr/src \
    && git clone https://github.com/google/googletest.git \
    && cd / \
    && git clone https://github.com/abseil/abseil-cpp.git \
    && cd abseil-cpp \
    && mkdir build \
    && cd build \
    && cmake -DABSL_BUILD_TESTING=ON -DBUILD_SHARED_LIBS=ON -DABSL_PROPAGATE_CXX_STD=ON -DBUILD_STATIC_LIBS=ON -DABSL_USE_GOOGLETEST_HEAD=ON -DCMAKE_CXX_STANDARD=14 -DCMAKE_INSTALL_PREFIX=/usr .. \
    && export PKG_CONFIG=/usr/lib/pkgconfig \
    && make all install \
    && git clone https://github.com/sass/libsass.git \
    && cd libsass \
    && echo "export SASS_LIBSASS_PATH=$(pwd)" >> ~/.bash_profile && mkdir /artifacts \
    # && ./sclsript/bootstrap \
    && export BUILD="shared" && make -B PREFIX=build 
WORKDIR /abseil-cpp/build/libsass
VOLUME '/artifacts'
RUN mv lib/libsass.so /artifacts/libsass.so && rm -r lib \
    && export BUILD="static" && make -B PREFIX=build \
    && mv lib/libsass.a /artifacts/libsass.a